#[derive(Debug)]
struct Node<T> {
    value: T,
    prev: usize,
    next: usize,
}

struct CircularLinkedList<T> {
    list: Vec<Node<T>>,
    size: usize,
}

impl<T> CircularLinkedList<T> {
    fn get(&self, i: usize) -> &T {
        let i_rem = i.rem_euclid(self.size);
        &self.list[i_rem].value
    }
    fn get_relative(&self, i: usize, steps: usize) -> &T {
        let steps_rem = steps.rem_euclid(self.size);
        let mut cursor = i;
        for _ in 0..steps_rem {
            cursor = self.list[cursor].next;
        }
        &self.list[cursor].value
    }
    fn move_relative(&mut self, i: usize, steps: i64) {
        if steps == 0 {
            return;
        }
        let steps_rem = steps.rem_euclid(self.size as i64 - 1);
        let mut cursor = i;
        for _ in 0..steps_rem {
            cursor = self.list[cursor].next
        }

        // remove from current place
        let neigh_left = self.list[i].prev;
        let neigh_right = self.list[i].next;
        self.list[neigh_left].next = self.list[i].next;
        self.list[neigh_right].prev = self.list[i].prev;

        // insert into new place
        let new_neigh_left = cursor;
        let new_neigh_right = self.list[cursor].next;
        self.list[new_neigh_left].next = i;
        self.list[new_neigh_right].prev = i;
        self.list[i].prev = new_neigh_left;
        self.list[i].next = new_neigh_right;
    }
}

impl<T> FromIterator<T> for CircularLinkedList<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut list: Vec<Node<T>> = iter
            .into_iter()
            .enumerate()
            .map(|(i, value)| Node {
                value,
                prev: i.saturating_sub(1),
                next: i.saturating_add(1),
            })
            .collect();
        let size = list.len();
        list[0].prev = size - 1;
        list[size - 1].next = 0;
        CircularLinkedList { list, size }
    }
}

fn main() {
    let mut list: CircularLinkedList<_> = std::io::stdin()
        .lines()
        .flatten()
        .map(|s| s.parse::<i64>())
        .flatten()
        .map(|n| n * 811589153) // PART 2
        .collect();

    for _ /* PART 2 */ in 0..10 {
        for i in 0..list.size {
            let &value = list.get(i);
            list.move_relative(i, value);
        }
    }

    let mut zero = 0;
    for i in 0..list.size {
        if *list.get(i) == 0 {
            zero = i
        }
    }

    let grove1 = list.get_relative(zero, 1000);
    let grove2 = list.get_relative(zero, 2000);
    let grove3 = list.get_relative(zero, 3000);

    println!(
        "We have coords ({},{},{}), summing to {}",
        grove1,
        grove2,
        grove3,
        grove1 + grove2 + grove3
    );
}
