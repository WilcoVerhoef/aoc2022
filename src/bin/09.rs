use std::collections::HashSet;

enum Dir {
    Up,
    Down,
    Left,
    Right,
}

use Dir::*;

fn main() {
    let mut seen = HashSet::from([(0, 0)]);

    let mut lines = std::io::stdin().lines();

    let nul: (i32, i32) = (0, 0);
    let mut positions = [nul; 10];

    while let Some(Ok(command)) = lines.next() {
        let direction = match command.chars().nth(0) {
            Some('U') => Up,
            Some('D') => Down,
            Some('L') => Left,
            Some('R') => Right,
            _ => unreachable!(),
        };
        let distance: i32 = command[2..].parse().unwrap();

        for _ in 0..distance {
            let (h, v) = match direction {
                Up => (0, 1),
                Down => (0, -1),
                Left => (-1, 0),
                Right => (1, 0),
            };

            positions[0].0 += h;
            positions[0].1 += v;
            for k in 1..positions.len() {
                while positions[k].0.abs_diff(positions[k - 1].0) > 1
                    || positions[k].1.abs_diff(positions[k - 1].1) > 1
                {
                    let xdiff = positions[k - 1].0.cmp(&positions[k].0) as i32;
                    let ydiff = positions[k - 1].1.cmp(&positions[k].1) as i32;
                    positions[k].0 += xdiff;
                    positions[k].1 += ydiff;
                    seen.insert(*positions.last().unwrap());
                }
            }
        }
    }

    println!("{}", seen.len());
}
