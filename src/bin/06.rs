fn main() {
    let mut input = String::new();
    _ = std::io::stdin().read_line(&mut input);

    'outer: for i in 0..input.len() {
        for j in i..i + 14 {
            for k in j + 1..i + 14 {
                if input.chars().nth(j) == input.chars().nth(k) {
                    continue 'outer;
                }
            }
        }
        println!("{}", i + 14);
    }
}
