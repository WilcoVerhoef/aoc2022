use std::{
    collections::HashSet,
    fmt::{Display, Error, Formatter},
};

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
enum Cell {
    FallingRock,
    StoppedRock,
    Air,
}

#[derive(Debug, PartialEq)]
enum Jet {
    Left,
    Right,
}

use Cell::*;
use Jet::*;

impl Display for Cell {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let char = match self {
            FallingRock => '@',
            StoppedRock => '#',
            Air => '.',
        };
        write!(f, "{}", char)
    }
}

struct Chamber {
    chamber: Vec<[Cell; 7]>,
    falling: Vec<(usize, usize)>,
}

impl Display for Chamber {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        for row in self.chamber.iter().rev().take(20) {
            write!(f, "|")?;
            for cell in row {
                write!(f, "{}", cell)?;
            }
            writeln!(f, "|")?;
        }
        writeln!(f, "+-------+")?;
        writeln!(f, "{:?}", self.falling)
    }
}

impl Chamber {
    fn push(&mut self, jet: Jet) -> bool {
        for (i, j) in &self.falling {
            if *j == 0 && jet == Left || *j == 6 && jet == Right {
                return false;
            }
            let j = match jet {
                Left => j - 1,
                Right => j + 1,
            };
            if self.chamber[*i][j] == StoppedRock {
                return false;
            }
        }

        for &(i, j) in &self.falling {
            self.chamber[i][j] = Air;
        }

        for k in 0..self.falling.len() {
            let (i, mut j) = self.falling[k];
            match jet {
                Left => j -= 1,
                Right => j += 1,
            }
            self.chamber[i][j] = FallingRock;
            self.falling[k].1 = j;
        }
        return true;
    }
    fn fall(&mut self) -> bool {
        if self
            .falling
            .iter()
            .any(|&(i, j)| i == 0 || self.chamber[i - 1][j] == StoppedRock)
        {
            for (i, j) in self.falling.drain(..) {
                self.chamber[i][j] = StoppedRock;
            }
            return false;
        }

        for &(i, j) in &self.falling {
            self.chamber[i][j] = Air;
        }
        for k in 0..self.falling.len() {
            let (i, j) = self.falling[k];
            self.chamber[i - 1][j] = FallingRock;
            self.falling[k].0 -= 1;
        }

        return true;
    }
    fn clean(&mut self) {
        while self.chamber.last() == Some(&[Air; 7]) {
            self.chamber.pop();
        }
    }
    fn new_rock(&mut self, rock: &Vec<[Cell; 7]>) {
        self.clean();
        self.chamber.append(&mut vec![[Air; 7]; 3]);
        for (i, row) in rock.iter().enumerate() {
            for (j, &cell) in row.iter().enumerate() {
                if cell == FallingRock {
                    self.falling.push((i + self.chamber.len(), j));
                }
            }
        }
        self.chamber.append(&mut rock.clone());
    }
}

fn parse_rock(str: &str) -> Vec<[Cell; 7]> {
    str.lines()
        .rev()
        .map(|line| {
            let mut s = [Air; 7];
            for (i, char) in line.chars().enumerate() {
                s[i + 2] = match char {
                    '.' => Air,
                    '#' => FallingRock,
                    _ => unreachable!(),
                };
            }
            s
        })
        .collect()
}

fn main() {
    let mut chamber = Chamber {
        chamber: Vec::new(),
        falling: Vec::new(),
    };

    let rocks = [
        "####",
        ".#.\n\
         ###\n\
         .#.",
        "..#\n\
         ..#\n\
         ###",
        "#\n\
         #\n\
         #\n\
         #",
        "##\n\
         ##",
    ]
    .map(parse_rock);

    let mut rocks = rocks.iter().cycle();

    let input = std::io::stdin()
        .lines()
        .map(|line| line.unwrap())
        .collect::<String>();
    println!("jet length: {}", input.len());
    let mut jets = input
        .chars()
        .map(|c| match c {
            '>' => Right,
            '<' => Left,
            _ => unreachable!(),
        })
        .cycle();

    let mut seen = HashSet::new();

    let mut rock_count = 0;
    loop {
        for i in 0..5 * input.len() {
            rock_count += 1;
            chamber.new_rock(rocks.next().unwrap());
            loop {
                chamber.push(jets.next().unwrap());
                if !chamber.fall() {
                    break;
                }
            }
            if rock_count == 14140745 {
                chamber.clean();
                println!(
                    "Done! {} rocks, with {} height:",
                    rock_count,
                    chamber.chamber.len()
                );
                println!("{}", chamber);
                return;
            }
        }
        chamber.clean();
        println!(
            "biggie after {} rocks, with {} height:",
            rock_count,
            chamber.chamber.len()
        );
        println!("{}", chamber);
        let x = chamber
            .chamber
            .iter()
            .rev()
            .take(20)
            .map(|x| x.iter().map(|c| *c).collect())
            .collect::<Vec<Vec<_>>>();
        if seen.contains(&x) {
            println!("we found one!!!");
            return;
        }
        seen.insert(x);
    }
}

// manually solve it:

/*

after 200 rocks -> height 308
after 1600 rocks -> height 2428

So, after the first 200, (height 308)
    another 714285714 cycles of 1400 (height 2120 each)
    and another 200 (300)

=========

after 50455 rocks -> height 80301
after 17659250 rocks -> height 28113099

cycle len: 17608795
cycle height diff: 28032798

So we need 14140745 rocks, + 56789 cycles

Done! 14140745 rocks, with 22511720
 and 56789 * 28032798 =

*/
