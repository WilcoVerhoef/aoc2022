use std::{collections::HashMap, num::ParseIntError, str::FromStr};

enum Expr {
    Term(i64),
    Add(String, String),
    Sub(String, String),
    Mult(String, String),
    Div(String, String),
}

impl FromStr for Expr {
    type Err = ParseIntError;
    fn from_str(s: &str) -> Result<Self, ParseIntError> {
        if !s.contains(' ') {
            let n = s.parse()?;
            return Ok(Expr::Term(n));
        }
        let mut splitted = s.split(" ");
        let lhs = splitted.next().unwrap().to_string();
        let op = splitted.next().unwrap();
        let rhs = splitted.next().unwrap().to_string();
        match op {
            "+" => Ok(Expr::Add(lhs, rhs)),
            "-" => Ok(Expr::Sub(lhs, rhs)),
            "*" => Ok(Expr::Mult(lhs, rhs)),
            "/" => Ok(Expr::Div(lhs, rhs)),
            _ => unreachable!(),
        }
    }
}

fn main() {
    let exprs = std::io::stdin()
        .lines()
        .flatten()
        .map(|l| {
            let (name, expr) = l.split_once(": ").unwrap();
            (name.to_string(), expr.parse::<Expr>().unwrap())
        })
        .collect::<HashMap<_, _>>();

    println!("PART 1: {}", calculate(&exprs, &String::from("root"), None));
    println!("PART 2: {}", part2(&exprs));
}

fn calculate(exprs: &HashMap<String, Expr>, name: &String, humn: Option<i64>) -> i64 {
    if *name == String::from("humn") && humn.is_some() {
        return humn.unwrap();
    }
    match exprs.get(name).unwrap() {
        Expr::Term(n) => *n,
        Expr::Add(a, b) => calculate(exprs, a, humn) + calculate(exprs, b, humn),
        Expr::Sub(a, b) => calculate(exprs, a, humn) - calculate(exprs, b, humn),
        Expr::Mult(a, b) => calculate(exprs, a, humn) * calculate(exprs, b, humn),
        Expr::Div(a, b) => calculate(exprs, a, humn) / calculate(exprs, b, humn),
    }
}

fn part2(exprs: &HashMap<String, Expr>) -> i64 {
    // part 2, using logarithmic binary search (or whatever this is called)
    let (lhs, rhs) = match exprs.get(&String::from("root")).unwrap() {
        Expr::Add(a, b) => (a, b),
        Expr::Sub(a, b) => (a, b),
        Expr::Mult(a, b) => (a, b),
        Expr::Div(a, b) => (a, b),
        Expr::Term(_) => unreachable!(),
    };

    let evaluate = |n| calculate(&exprs, lhs, Some(n)) - calculate(&exprs, rhs, Some(n));

    // double the guess until we flip sign
    let mut guess = 1;
    let zero = evaluate(0);
    while evaluate(guess).signum() == zero.signum() {
        guess *= 2;
    }

    // binary search for the actual value
    let mut l_bound = guess / 2;
    let mut r_bound = guess;
    let mut l_sign = evaluate(l_bound).signum();
    let mut r_sign = evaluate(r_bound).signum();

    while l_bound < r_bound {
        let mid = (l_bound + r_bound) / 2;
        let mid_sign = evaluate(mid).signum();
        if mid_sign == 0 {
            return mid;
        }
        if mid_sign == l_sign {
            l_bound = mid;
            l_sign = mid_sign;
            continue;
        }
        if mid_sign == r_sign {
            r_bound = mid;
            r_sign = mid_sign;
            continue;
        }
    }

    return l_bound;
}
