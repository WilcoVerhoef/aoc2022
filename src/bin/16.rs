use petgraph::{
    algo::{dijkstra, floyd_warshall, toposort},
    dot::{Config, Dot},
    graphmap::DiGraphMap,
    prelude::{DiGraph, UnGraph, UnGraphMap},
    visit::Topo,
    Direction::Incoming,
};
use std::{collections::HashMap, ops::Range, str::FromStr};

#[derive(Debug)]
struct Valve {
    name: String,
    flow_rate: i32,
    tunnels: Vec<String>,
}

impl FromStr for Valve {
    type Err = ();
    fn from_str(line: &str) -> Result<Valve, ()> {
        let line = line.trim_start_matches("Valve ");
        let (name, line) = line.split_once(" ").ok_or(())?;
        let line = line.trim_start_matches("has flow rate=");
        let (flow_rate, line) = line.split_once("; ").ok_or(())?;
        let line = line.trim_start_matches("tunnels lead to valves ");
        let line = line.trim_start_matches("tunnel leads to valve ");
        let tunnels = line.split(", ");

        Ok(Valve {
            name: name.to_string(),
            flow_rate: flow_rate.parse().map_err(|_| ())?,
            tunnels: tunnels.map(|t| t.to_string()).collect(),
        })
    }
}

type Timestamp = i32;
type ValveIx = i32;
// trait ValveIxMethods {}
// impl ValveIxMethods for ValveIx {}

type ValveState = i32;
trait ValveStateMethods {
    fn open(&self, valve: ValveIx) -> ValveState;
    fn is_open(&self, valve: ValveIx) -> bool;
    fn possibilities(num_valves: u32) -> Range<ValveState>;
}
impl ValveStateMethods for ValveState {
    fn open(&self, valve: ValveIx) -> ValveState {
        self | (2_i32.pow(valve as u32))
    }
    fn is_open(&self, valve: ValveIx) -> bool {
        self & (2_i32.pow(valve as u32)) > 0
    }
    fn possibilities(num_valves: u32) -> Range<ValveState> {
        let two: ValveState = 2;
        0..two.pow(num_valves)
    }
}

fn input() -> HashMap<String, Valve> {
    let lines = std::io::stdin().lines().flatten();

    lines
        .map(|line| line.parse::<Valve>())
        .flatten()
        .map(|x| (x.name.clone(), x))
        .collect()
}

fn build_graph(
    valves: HashMap<String, Valve>,
) -> (
    DiGraphMap<(ValveIx, ValveState, Timestamp), i32>,
    Vec<(String, Valve)>,
) {
    let edges = valves.iter().flat_map(|(name, valve)| {
        valve
            .tunnels
            .iter()
            .map(|tunnel| (name.as_str(), tunnel.as_str()))
    });
    let all_valves_graph: DiGraphMap<_, ()> = edges.collect();

    // do a search from each "special" node to eachother "special" node.
    println!("Valves: {:?}", valves);

    let cool_valves = valves
        .iter()
        .filter(|(name, valve)| valve.flow_rate > 0)
        .collect::<Vec<_>>();

    println!(
        "we have {} cool valves of {} total valves",
        cool_valves.len(),
        valves.len()
    );

    println!(
        "cool valves: {:?}",
        cool_valves.iter().map(|(s, _)| s).collect::<Vec<_>>()
    );

    println!(
        "{:?}",
        Dot::with_config(&all_valves_graph, &[Config::EdgeNoLabel])
    );
    // lets do all-pair-shortest-path
    let dists = floyd_warshall(&all_valves_graph, |_| 1).unwrap();

    let mut cool_edges = Vec::new();

    for i in 0..cool_valves.len() {
        for j in 0..cool_valves.len() {
            if i == j {
                continue;
            };
            let valve_i = cool_valves[i].0.as_str();
            let valve_j = cool_valves[j].0.as_str();
            let &dist = dists.get(&(valve_i, valve_j)).unwrap();
            cool_edges.push((i as i32, j as i32, dist));
        }
    }
    println!("We have {} cool edges: {:?}", cool_edges.len(), cool_edges);

    // let cool_edges = cool_valves
    //     .iter()
    //     .enumerate()
    //     .flat_map(|(index, (name, _))| {
    //         dijkstra(&all_valves_graph, name, None, |_| 1)
    //             .iter()
    //             .filter(|(other, _)| cool_valves.iter().any(|(name, _)| name == other)) // filter out the ones that don't lead to another cool node
    //             .map(|(other, dist)| {
    //                 (
    //                     index as i32,
    //                     cool_valves
    //                         .iter()
    //                         .position(|(name, _)| name == other)
    //                         .unwrap() as i32,
    //                     *dist,
    //                 )
    //             })
    //             .collect::<Vec<_>>()
    //     });

    // now we want to "explode" this graph.
    // let possibilities = || ValveState::possibilities(cool_valves.len() as u32);

    // check wether this works
    let x = (0..(2 as ValveState).pow(5))
        .filter(|state| state.is_open(2))
        .map(|s| println!("state {:b}", s))
        .collect::<Vec<_>>();
    println!("those are {} different states", x.len());

    // first; the "tunnel" edges, but 8000 of them
    // edit: lets combine them with the "open valve" edges!!
    let all_tunnels = cool_edges
        .iter()
        .flat_map(|(from, to, dist)| {
            println!(
                "from {} to {} dist {}",
                cool_valves[*from as usize].0, cool_valves[*to as usize].0, dist
            );
            (0..(2 as ValveState).pow(cool_valves.len() as u32))
                .filter(|state| state.is_open(*from) && !state.is_open(*to))
                .flat_map(|state| {
                    (0..31 - dist - 1)
                        .map(|t| {
                            (
                                (*from, state, t),
                                (*to, state.open(*to), t + dist + 1),
                                dist + 1,
                            )
                        })
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    // but we do need edges from AA to each special node!
    let dists_from_start = dijkstra(&all_valves_graph, "AA", None, |_| 1);
    let start_edges = cool_valves
        .iter()
        .enumerate()
        .map(|(index, (name, valve))| {
            (
                (131, 0, 0),
                (
                    index as i32,
                    0.open(index as i32),
                    *dists_from_start.get(name.as_str()).unwrap() + 1 + 4,
                ),
                *dists_from_start.get(name.as_str()).unwrap() + 1 + 4, // 4 is the "elephant teaching time"
            )
        })
        .collect::<Vec<_>>();

    println!(
        "We have {} start_edges, and {} tunnels",
        start_edges.len(),
        all_tunnels.len()
    );

    // and finally, create that big graph!!
    let all_edges = start_edges.iter().chain(all_tunnels.iter());
    (
        // start_edges.chain(&all_tunnels).collect(),
        all_edges.collect(),
        cool_valves
            .iter()
            .map(|&(s, v)| {
                (
                    s.clone(),
                    Valve {
                        name: v.name.clone(),
                        flow_rate: v.flow_rate,
                        tunnels: v.tunnels.clone(),
                    },
                )
            })
            .collect(),
    )
}

fn search_graph(dag: DiGraphMap<(ValveIx, ValveState), i32>) {
    unimplemented!()
}

fn main() {
    let valves = input();
    let (states, cool_valves) = build_graph(valves);
    println!(
        "done! Build a graph with {} nodes and {} edges, and it is ?? cyclic",
        states.node_count(),
        states.edge_count(),
    );

    let mut max_scores: HashMap<(ValveIx, ValveState, Timestamp), i32> = HashMap::new();
    let mut the_max_score = 0;

    let mut counter = 0;

    let mut topo = Topo::new(&states);
    while let Some((ix, state, time)) = topo.next(&states) {
        counter += 1;
        if ix == 131 {
            println!("handling AA");
            max_scores.insert((131, 0, 0), 0);
            continue;
        }
        if counter % 10000 == 0 {
            println!("counter is at {}", counter);
        }
        // let mut pareto = [i32::MIN; max_min];
        let incoming = states.edges_directed((ix, state, time), Incoming);
        let mut m = i32::MIN;
        for ((from, old_state, old_t), (to, new_state, new_t), dist) in incoming {
            let max_from = max_scores.get(&(from, old_state, old_t)).unwrap();
            let score_inc = (30 - new_t) * cool_valves[to as usize].1.flow_rate;
            m = m.max(max_from + score_inc);
            // for i in 0..(max_min as i32) - dist {
            //     // let new_time = (i + dist) as usize;
            //     // let score_inc = (30 - new_time as i32) * cool_valves[to as usize].1.flow_rate;

            //     pareto[new_time] = pareto[new_time].max(pareto_from[i as usize] + score_inc);
            //     the_max_score = the_max_score.max(pareto[new_time]);
            // }
        }
        max_scores.insert((ix, state, time), m);
        the_max_score = the_max_score.max(m);

        // if ix == dd_ix as i32 && state == 0.open(dd_ix as i32) {
        //     println!("ok, doing the DD now, score {:?}", pareto);
        // }
    }
    println!(
        "Done traversing all incomming edges, max_score {}. Counter is {}",
        the_max_score, counter
    );

    // lets generate a max-per-final-state array.
    let mut maxes = vec![0; (2 as ValveState).pow(cool_valves.len() as u32) as usize];
    for ((ix, state, time), score) in max_scores {
        maxes[state as usize] = maxes[state as usize].max(score);
    }

    let mut the_max_2 = 0;
    // now find the best among all combinations
    for state in ValveState::possibilities(cool_valves.len() as u32) {
        if state % 100 == 0 {
            println!("last step at state {}", state)
        }
        for other_state in ValveState::possibilities(cool_valves.len() as u32) {
            if state & other_state > 0 {
                continue;
            }
            the_max_2 = the_max_2.max(maxes[state as usize] + maxes[other_state as usize]);
        }
    }
    println!("And the max for part 2 is {}", the_max_2);
}
