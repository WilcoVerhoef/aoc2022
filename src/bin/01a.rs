// use std::slice::Split;

fn main() {
    let lines = std::io::stdin().lines();
    let mut max = 0;
    let mut count: i32 = 0;
    for line in lines {
        match line.unwrap().parse() {
            Ok(n) => {
                let _: i32 = n;
                count += n;
            }
            Err(_) => {
                max = std::cmp::max(max, count);
                count = 0;
            }
        }
    }
    max = std::cmp::max(max, count);
    println!("{}", max);
}
