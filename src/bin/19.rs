use good_lp::{constraint, default_solver, variables, Expression, Solution, SolverModel};
use regex::Regex;
use std::str::FromStr;
use Resource::*;

enum Resource {
    Ore,
    Clay,
    Obsidian,
    Geode,
}

struct Blueprint {
    id: i32,
    costs: [[i32; 4]; 4],
}

impl FromStr for Blueprint {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, ()> {
        let re = Regex::new(
            r"^Blueprint (\d+): Each ore robot costs (\d+) ore\. Each clay robot costs (\d+) ore\. Each obsidian robot costs (\d+) ore and (\d+) clay\. Each geode robot costs (\d+) ore and (\d+) obsidian\.$",
        ).unwrap();

        let caps = re.captures(s).unwrap();
        let mut costs = [[0; 4]; 4];

        let id = caps.get(1).unwrap().as_str().parse().unwrap();
        costs[Ore as usize][Ore as usize] = caps.get(2).unwrap().as_str().parse().unwrap();
        costs[Clay as usize][Ore as usize] = caps.get(3).unwrap().as_str().parse().unwrap();
        costs[Obsidian as usize][Ore as usize] = caps.get(4).unwrap().as_str().parse().unwrap();
        costs[Obsidian as usize][Clay as usize] = caps.get(5).unwrap().as_str().parse().unwrap();
        costs[Geode as usize][Ore as usize] = caps.get(6).unwrap().as_str().parse().unwrap();
        costs[Geode as usize][Obsidian as usize] = caps.get(7).unwrap().as_str().parse().unwrap();

        println!("{:?}", costs);
        Ok(Blueprint { id, costs })
    }
}

impl Blueprint {
    fn get_costs(&self, robot: Resource) -> [i32; 4] {
        self.costs[robot as usize]
    }
}

// type State =

fn main() {
    println!("Hello, world!");

    let blueprints: Vec<Blueprint> = std::io::stdin()
        .lines()
        .flatten()
        .map(|x| x.parse())
        .flatten()
        .collect();

    let mut total_quality = 0;
    for print in blueprints.iter() {
        let qa = quality_level(print, 24);
        println!("blueprint {}: scored {}", print.id, qa);
        total_quality += qa * print.id;
    }

    println!("PART 1: total quality multiplied: {}", total_quality);

    let first = quality_level(&blueprints[0], 32);
    let second = quality_level(&blueprints[1], 32);
    let third = quality_level(&blueprints[2], 32);

    println!(
        "PART 1 (again): total quality multiplied: {}",
        total_quality
    );
    println!(
        "PART 2: qualities for part 2: {} {} {}, multi: {}",
        first,
        second,
        third,
        first * second * third
    );
}

fn quality_level(blueprint: &Blueprint, max_time: usize) -> i32 {
    variables! {
        vars:
            construct_ore[max_time] (binary);
            construct_clay[max_time] (binary);
            construct_obsidian[max_time] (binary);
            construct_geode[max_time] (binary);
    }

    let objective: good_lp::Expression = construct_geode
        .iter()
        .enumerate()
        .map(|(i, &b)| ((max_time - 1 - i) as i32) * b)
        .sum();

    let mut problem = vars.maximise(objective.clone()).using(default_solver);

    // only create a single robot per slot
    for t in 0..max_time {
        problem = problem.with(constraint!(
            construct_ore[t] + construct_clay[t] + construct_obsidian[t] + construct_geode[t] <= 1
        ));
    }

    // make sure to only construct when enough resources

    for t in 0..max_time {
        for mineral in 0..4 {
            let mut cost = Expression::from(0);
            for k in 0..t + 1 {
                cost += construct_ore[k] * blueprint.costs[Ore as usize][mineral];
                cost += construct_clay[k] * blueprint.costs[Clay as usize][mineral];
                cost += construct_obsidian[k] * blueprint.costs[Obsidian as usize][mineral];
                cost += construct_geode[k] * blueprint.costs[Geode as usize][mineral];
            }

            let mut yild = Expression::from(if mineral == 0 { t as i32 } else { 0 });
            if t > 0 {
                for k in 0..t - 1 {
                    match mineral {
                        0 => yild += construct_ore[k] * ((t - k) as i32 - 1),
                        1 => yild += construct_clay[k] * ((t - k) as i32 - 1),
                        2 => yild += construct_obsidian[k] * ((t - k) as i32 - 1),
                        3 => yild += construct_geode[k] * ((t - k) as i32 - 1),
                        _ => unreachable!(),
                    }
                }
            }

            problem = problem.with(constraint!(cost <= yild));
        }
    }

    let solution = problem.solve().unwrap();

    // for t in 0..max_time {
    //     println!(
    //         "{}:: ore: {}, clay: {}, obs: {}, geode: {}",
    //         t + 1,
    //         solution.value(construct_ore[t]) as i32,
    //         solution.value(construct_clay[t]) as i32,
    //         solution.value(construct_obsidian[t]) as i32,
    //         solution.value(construct_geode[t]) as i32,
    //     )
    // }

    solution.eval(objective).round() as i32
}
