use std::{
    cmp::Ordering,
    iter::Peekable,
    str::{Chars, FromStr},
};

#[derive(Eq, PartialEq, Debug)]
enum Packet {
    Integer(i32),
    List(Vec<Packet>),
}

use Packet::*;

impl FromStr for Packet {
    type Err = ();

    fn from_str(s: &str) -> Result<Packet, ()> {
        fn parse_packet(cs: &mut Peekable<Chars>) -> Packet {
            if cs.peek() == Some(&',') {
                cs.next();
            }
            match cs.peek() {
                Some(&'[') => {
                    cs.next();
                    let mut values = Vec::new();
                    while cs.peek() != Some(&']') {
                        values.push(parse_packet(cs));
                        if cs.peek() == Some(&',') {
                            cs.next();
                        }
                    }
                    cs.next();
                    List(values)
                }
                Some(c) if c.is_numeric() => {
                    let mut value: String = String::new();
                    while let Some(c) = cs.peek().filter(|x| x.is_numeric()) {
                        value.push(*c);
                        cs.next();
                    }
                    Integer(value.parse().unwrap())
                }
                _ => unreachable!(),
            }
        }
        Ok(parse_packet(&mut s.chars().peekable()))
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Packet {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Integer(x), Integer(y)) => x.cmp(y),
            (List(xs), List(ys)) => xs.cmp(ys),
            (Integer(x), List(ys)) => (&vec![Integer(*x)]).cmp(ys),
            (List(xs), Integer(y)) => xs.cmp(&vec![Integer(*y)]),
        }
    }
}

fn main() {
    let mut i = 1;
    let mut counter = 0;
    let mut lines = std::io::stdin().lines();

    let mut all_of_them = Vec::new();

    while let (Some(Ok(a)), Some(Ok(b)), _) = (lines.next(), lines.next(), lines.next()) {
        let pack_a: Packet = a.parse().unwrap();
        let pack_b: Packet = b.parse().unwrap();
        if pack_a < pack_b {
            println!("{}", i);
            counter += i;
        }
        i += 1;

        all_of_them.push(pack_a);
        all_of_them.push(pack_b);
    }
    println!("{}", counter);

    all_of_them.push("[[2]]".parse().unwrap());
    all_of_them.push("[[6]]".parse().unwrap());

    all_of_them.sort_unstable();

    let index2 = all_of_them
        .iter()
        .position(|x| *x == "[[2]]".parse().unwrap())
        .unwrap()
        + 1;
    let index6 = all_of_them
        .iter()
        .position(|x| *x == "[[6]]".parse().unwrap())
        .unwrap()
        + 1;

    println!("{} {} {}", index2, index6, index2 * index6);
}
