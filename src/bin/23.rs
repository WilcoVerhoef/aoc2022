use std::collections::HashSet;

fn main() {
    let mut elves = parse_input();

    let directions = [North, South, West, East];

    let mut i = 0;
    loop {
        let dirs = directions.iter().cycle().skip(i).take(4).collect();
        i += 1;
        let moved;
        (elves, moved) = do_round(elves, dirs);
        println!("Round {}, moved {} elves", i, moved);
        if moved == 0 {
            break;
        }
    }

    let xs = elves.iter().map(|p| p.x);
    let ys = elves.iter().map(|p| p.y);
    let x_span = 1 + xs.clone().max().unwrap() - xs.min().unwrap();
    let y_span = 1 + ys.clone().max().unwrap() - ys.min().unwrap();
    println!(
        "Done {} iterations! The bounding box is {}×{}={},
        but with the elves taking up {} there are {} empty places",
        i,
        x_span,
        y_span,
        x_span * y_span,
        elves.len(),
        x_span * y_span - elves.len() as i32
    );
}

fn do_round(elves: HashSet<Pos>, preferred_dirs: Vec<&Dir>) -> (HashSet<Pos>, i32) {
    let mut moved_elves = HashSet::new();
    let mut number_moved = 0;

    'next_elve: for &elve in elves.iter() {
        if elve.look_around().iter().any(|p| elves.contains(p)) {
            for &&dir in preferred_dirs.iter() {
                if elve.look_dir(dir).iter().all(|look| !elves.contains(look)) {
                    // looks empty! now lets see if it's the first elve to walk into this pos.
                    let new_pos = elve.move_dir(dir);
                    if moved_elves.remove(&new_pos) {
                        // oh no, another elve wanted to go here, lets push them back!
                        moved_elves.insert(new_pos.move_dir(dir));
                        moved_elves.insert(elve);
                        number_moved -= 1;
                    } else {
                        number_moved += 1;
                        moved_elves.insert(new_pos);
                    }
                    continue 'next_elve;
                }
            }
        }
        // all directions are blocked! just stand where we are
        moved_elves.insert(elve);
    }

    assert_eq!(elves.len(), moved_elves.len());
    assert!(number_moved >= 0);

    (moved_elves, number_moved)
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct Pos {
    x: i32,
    y: i32,
}

impl Pos {
    fn move_dir(&self, dir: Dir) -> Self {
        let Pos { x, y } = *self;
        match dir {
            North => Pos { x: x - 1, y },
            South => Pos { x: x + 1, y },
            West => Pos { x, y: y - 1 },
            East => Pos { x, y: y + 1 },
        }
    }
    fn look_dir(&self, dir: Dir) -> [Self; 3] {
        match dir {
            North => [
                self.move_dir(North),
                self.move_dir(North).move_dir(West),
                self.move_dir(North).move_dir(East),
            ],
            South => [
                self.move_dir(South),
                self.move_dir(South).move_dir(West),
                self.move_dir(South).move_dir(East),
            ],
            West => [
                self.move_dir(West),
                self.move_dir(West).move_dir(North),
                self.move_dir(West).move_dir(South),
            ],
            East => [
                self.move_dir(East),
                self.move_dir(East).move_dir(North),
                self.move_dir(East).move_dir(South),
            ],
        }
    }
    fn look_around(&self) -> [Self; 8] {
        [
            self.move_dir(North),
            self.move_dir(North).move_dir(East),
            self.move_dir(East),
            self.move_dir(South).move_dir(East),
            self.move_dir(South),
            self.move_dir(South).move_dir(West),
            self.move_dir(West),
            self.move_dir(North).move_dir(West),
        ]
    }
}

#[derive(Clone, Copy)]
enum Dir {
    North,
    South,
    West,
    East,
}

use Dir::*;

fn parse_input() -> HashSet<Pos> {
    let lines = std::io::stdin().lines().flatten();

    let mut elves = HashSet::new();

    for (x, line) in lines.enumerate() {
        for (y, char) in line.char_indices() {
            if char == '#' {
                elves.insert(Pos {
                    x: x as i32,
                    y: y as i32,
                });
            }
        }
    }

    elves
}
