fn main() {
    let mut total = 0;
    let mut fill = 44804833;
    let mut needed = 30000000 - (70000000 - fill);
    let mut smallestBig = 30000000;
    let mut stack: Vec<i32> = Vec::new();
    for line in std::io::stdin().lines() {
        println!("{:?}", stack);
        println!("{:?}", line);
        let l = line.unwrap();
        let s: Vec<_> = l.split(" ").collect();
        match s[0].parse() {
            Ok(n) => {
                let _: i32 = n;
                let last = stack.len() - 1;
                stack[last] += n;
            }
            Err(n) => {
                if s.len() > 2 && s[2] == ".." {
                    let last = stack.len() - 1;
                    stack[last - 1] += stack[last];
                    let out = stack.pop().unwrap();
                    if out <= 100000 {
                        total += out;
                    }
                    if out >= needed && out < smallestBig {
                        smallestBig = out;
                    }
                } else if s[1] == "cd" {
                    stack.push(0);
                }
            }
        }
    }
    println!("{}", total);
    println!("{:?}", stack);
    println!("{}", smallestBig);
}
