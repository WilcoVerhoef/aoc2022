use eval;

struct Monkey {
    items: Vec<u64>,
    operation: String,
    test: u64,
    true_to: usize,
    false_to: usize,
    inspected: usize,
}

impl Monkey {
    fn operate(&self, old: u64) -> u64 {
        let x = 0b100000000000000000000000000000000;
        if old > x {
            println!("overflow warning, {} {}", old, self.operation);
        }
        eval::Expr::new(self.operation.clone())
            .value("old", old)
            .exec()
            .unwrap()
            .as_i64()
            .unwrap() as u64
    }

    fn next(&self, item: u64) -> bool {
        item % self.test == 0
    }
}

fn main() {
    let (mut monkeys, modulus) = input();
    println!("{}", modulus);
    for _ in 0..10000 {
        for i in 0..monkeys.len() {
            let true_to = monkeys[i].true_to;
            let false_to = monkeys[i].false_to;
            monkeys[i].inspected += monkeys[i].items.len();
            let transformed_items = monkeys[i]
                .items
                .iter()
                .map(|&k| monkeys[i].operate(k) % modulus);
            let (mut truths, mut falses): (Vec<_>, Vec<_>) =
                transformed_items.partition(|&k| monkeys[i].next(k));
            monkeys[true_to].items.append(&mut truths);
            monkeys[false_to].items.append(&mut falses);
            monkeys[i].items.clear();
        }
    }
    let mut inspectors: Vec<_> = monkeys.iter().map(|m| m.inspected).collect();
    inspectors.sort();
    inspectors.reverse();

    for (i, monkey) in monkeys.iter().enumerate() {
        println!("monkey {}: {:?} ({})", i, monkey.items, monkey.inspected);
    }

    println!("{:?}", inspectors[0] * inspectors[1]);
}

fn input() -> (Vec<Monkey>, u64) {
    let mut monkeys: Vec<Monkey> = Vec::new();
    let mut test_product = 1;
    let mut lines = std::io::stdin().lines();
    while let Some(Ok(line)) = lines.next() {
        if line.starts_with("Monkey") {
            let items: Vec<u64> = lines
                .next()
                .unwrap()
                .unwrap()
                .trim_start_matches("  Starting items: ")
                .split(", ")
                .map(|s| s.parse().unwrap())
                .collect();
            let operation = lines
                .next()
                .unwrap()
                .unwrap()
                .trim_start_matches("  Operation: new = ")
                .to_string();
            let test: u64 = lines
                .next()
                .unwrap()
                .unwrap()
                .trim_start_matches("  Test: divisible by ")
                .parse()
                .unwrap();
            test_product *= test;
            let true_to = lines
                .next()
                .unwrap()
                .unwrap()
                .trim_start_matches("    If true: throw to monkey ")
                .parse()
                .unwrap();
            let false_to = lines
                .next()
                .unwrap()
                .unwrap()
                .trim_start_matches("    If false: throw to monkey ")
                .parse()
                .unwrap();
            monkeys.push(Monkey {
                items,
                operation,
                test,
                true_to,
                false_to,
                inspected: 0,
            })
        }
    }
    return (monkeys, test_product);
}
