// use std::slice::Split;

fn main() {
    let lines = std::io::stdin().lines();
    let mut sums = vec![];
    let mut count = 0;
    for line in lines {
        match line.unwrap().parse() {
            Ok(n) => {
                let _: i32 = n;
                count += n;
            }
            Err(_) => {
                sums.push(count);
                count = 0;
            }
        }
    }
    sums.sort_unstable();
    sums.reverse();
    let sum: i32 = sums.iter().take(3).sum();
    println!("{}", sum);
}
