#[derive(Clone, PartialEq)]
enum Item {
    Air,
    Rock,
    Sand,
}

use Item::*;

fn main() {
    let mut paths = Vec::new();
    let mut max_depth = 0;

    let mut lines = std::io::stdin().lines();
    while let Some(Ok(line)) = lines.next() {
        let coords: Vec<_> = line
            .split(" -> ")
            .map(|c| {
                let mut coords = c.split(",");
                let x: usize = coords.next().unwrap().parse().unwrap();
                let y: usize = coords.next().unwrap().parse().unwrap();
                if y > max_depth {
                    max_depth = y
                }
                (x, y)
            })
            .collect();
        paths.push(coords);
    }

    // we now have all paths

    let mut grid = vec![vec![Air; 500]; 1000];

    for path in paths {
        let mut here = path[0];
        for target in path {
            grid[here.0][here.1] = Rock;
            while here != target {
                if target.0 > here.0 {
                    here.0 += 1;
                }
                if target.0 < here.0 {
                    here.0 -= 1;
                }
                if target.1 > here.1 {
                    here.1 += 1;
                }
                if target.1 < here.1 {
                    here.1 -= 1;
                }
                grid[here.0][here.1] = Rock;
            }
        }
    }

    for i in 0..1000 {
        grid[i][max_depth + 2] = Rock;
    }

    // print the empty grid

    for col in grid.iter() {
        for cell in col {
            match cell {
                Air => print!("."),
                Rock => print!("#"),
                Sand => print!("o"),
            }
        }
        println!("");
    }

    // run the simulation
    let mut i = 0;
    loop {
        let mut sandpos = (500, 0);
        while sandpos.1 < 200 {
            if grid[sandpos.0][sandpos.1 + 1] == Air {
                sandpos.1 += 1;
            } else if grid[sandpos.0 - 1][sandpos.1 + 1] == Air {
                sandpos.0 -= 1;
                sandpos.1 += 1;
            } else if grid[sandpos.0 + 1][sandpos.1 + 1] == Air {
                sandpos.0 += 1;
                sandpos.1 += 1;
            } else {
                grid[sandpos.0][sandpos.1] = Sand;
                break;
            }
        }
        if sandpos == (500, 0) {
            break;
        }
        if sandpos.1 > 190 {
            break;
        }
        i += 1;
    }

    println!("{}", i);
}
