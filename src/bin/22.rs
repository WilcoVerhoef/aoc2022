const SIDELENGTH: usize = 50;

fn main() {
    let (tiles, commands) = input();

    let first_open = tiles[0].iter().position(|x| x == &OpenTile).unwrap();
    let mut pos = Position {
        row: 0,
        column: first_open,
        facing: Right,
    };

    for command in commands.clone() {
        pos.apply(&tiles, command);
    }

    println!(
        "Position after all commands: {},{} (facing {:?}). This makes the password {}",
        pos.row + 1,
        pos.column + 1,
        pos.facing,
        1000 * (pos.row + 1) + 4 * (pos.column + 1) + pos.facing as usize
    );

    // PART 2

    let cube = Cube::from(tiles);

    // println!("{:?}", cube);

    let mut pos = Position {
        row: 0,
        column: 0,
        facing: Right,
    };

    let mut face = Face::A;

    for command in commands {
        match command {
            TurnRight => pos.facing = pos.facing.clockwise(),
            TurnLeft => pos.facing = pos.facing.counter_clockwise(),
            GoForward(n) => {
                for _ in 0..n {
                    let (next_face, next_pos) = Cube::step(face, pos.clone());
                    if cube.faces[next_face as usize][next_pos.row][next_pos.column] == OpenTile {
                        face = next_face;
                        pos = next_pos;
                    }
                }
            }
        }
    }

    println!(
        "Position after all commands (relative to face {:?}): {},{} (facing {:?}). Remember to add/subtract a multiple of {} depending on the original face pos!",
        face,
        pos.row+1,
        pos.column+1,
        pos.facing,
        SIDELENGTH,
    );
}

#[derive(PartialEq, Clone, Copy)]
enum Tile {
    OpenTile,
    SolidWall,
    EmptySpace,
}

impl std::fmt::Debug for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            OpenTile => write!(f, ".")?,
            SolidWall => write!(f, "#")?,
            EmtpySpace => write!(f, " ")?,
        }
        Ok(())
    }
}

#[derive(Clone, Copy)]
enum Command {
    TurnRight,
    TurnLeft,
    GoForward(i32),
}

#[derive(Clone, Copy, Debug)]
enum Direction {
    Right = 0,
    Down = 1,
    Left = 2,
    Up = 3,
}

impl Direction {
    fn clockwise(self) -> Self {
        match self {
            Right => Down,
            Down => Left,
            Left => Up,
            Up => Right,
        }
    }
    fn counter_clockwise(self) -> Self {
        match self {
            Right => Up,
            Down => Right,
            Left => Down,
            Up => Left,
        }
    }
    fn turn(self, by: i32) -> Self {
        let mut turned = self;
        for _ in 0..by.rem_euclid(4) {
            turned = turned.clockwise();
        }
        turned
    }
    fn diff(self, other: Self) -> i32 {
        self as i32 - other as i32
    }
}

#[derive(Clone, Copy)]
struct Position {
    row: usize,
    column: usize,
    facing: Direction,
}

impl Position {
    fn apply(&mut self, tiles: &Vec<Vec<Tile>>, command: Command) {
        match command {
            TurnRight => self.facing = self.facing.clockwise(),
            TurnLeft => self.facing = self.facing.counter_clockwise(),
            GoForward(n) => {
                for _ in 0..n {
                    self.step(tiles)
                }
            }
        }
    }
    fn step(&mut self, tiles: &Vec<Vec<Tile>>) {
        let x_mod = tiles.len();
        let y_mod = tiles[self.row].len();
        let next = |(x, y): (usize, usize)| match self.facing {
            Right => (x, (y + 1) % y_mod),
            Down => ((x + 1) % x_mod, y),
            Left => (x, (y_mod + y - 1) % y_mod),
            Up => ((x_mod + x - 1) % x_mod, y),
        };
        let (mut x, mut y) = (self.row, self.column);
        loop {
            (x, y) = next((x, y));
            let tile = tiles.get(x).and_then(|row| row.get(y));
            if tile == Some(&SolidWall) {
                return;
            }
            if tile == Some(&OpenTile) {
                self.row = x;
                self.column = y;
                return;
            }
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Face {
    A = 0,
    B = 1,
    C = 2,
    D = 3,
    E = 4,
    F = 5,
    /*
      +-+
      |A|
    +-+-+-+
    |B|C|D|
    +-+-+-+
      |E|
      +-+
      |F|
      +-+
    */
}

#[derive(Default)]
struct Cube {
    faces: [Vec<Vec<Tile>>; 6],
}

impl Cube {
    fn from(tiles: Vec<Vec<Tile>>) -> Cube {
        let mut mapmap: [[Option<Vec<Vec<Tile>>>; 6]; 6] = Default::default();

        for (i, facerow) in tiles.chunks(SIDELENGTH).enumerate() {
            let mut chunked_row = facerow
                .iter()
                .map(|x| x.chunks(SIDELENGTH))
                .collect::<Vec<_>>();
            let mut j = 0;
            while let Some(nextface) = chunked_row
                .iter_mut()
                .map(|c| c.next())
                .collect::<Option<Vec<_>>>()
            {
                if nextface
                    .iter()
                    .flat_map(|x| x.iter())
                    .all(|c| *c == EmptySpace)
                {
                    // this face is empty
                    j += 1;
                    continue;
                }
                mapmap[i][j] = Some(nextface.iter().map(|&row| row.into()).collect());
                j += 1;
            }
        }

        let mut cube: Cube = Default::default();
        let first = mapmap[0].iter().position(|x| x.is_some()).unwrap();

        let mut stack: Vec<((usize, usize), Face, Direction)> = vec![((0, first), Face::A, Right)];

        while let Some(((x, y), f, d)) = stack.pop() {
            println!("looking at {},{}", x, y);
            match mapmap
                .get(x)
                .and_then(|row| row.get(y))
                .and_then(|x| x.clone())
            {
                Some(a) => {
                    let a = Cube::rotate(&a, -(d as i32));
                    cube.faces[f as usize] = a;
                    println!("found face {:?} at {},{} (direction {:?})", f, x, y, d);
                }
                None => continue,
            }
            mapmap[x][y] = None;

            let (next_face, next_dir) = Cube::wrap(f, Right.turn(-(d as i32)));
            let new_d = d.turn(-next_dir.diff(Right.turn(-(d as i32))));
            stack.push(((x, y + 1), next_face, new_d));

            let (next_face, next_dir) = Cube::wrap(f, Down.turn(-(d as i32)));
            let new_d = d.turn(-next_dir.diff(Down.turn(-(d as i32))));
            stack.push(((x + 1, y), next_face, new_d));

            let (next_face, next_dir) = Cube::wrap(f, Left.turn(-(d as i32)));
            let new_d = d.turn(-next_dir.diff(Left.turn(-(d as i32))));
            stack.push(((x, y.wrapping_sub(1)), next_face, new_d));

            let (next_face, next_dir) = Cube::wrap(f, Up.turn(-(d as i32)));
            let new_d = d.turn(-next_dir.diff(Up.turn(-(d as i32))));
            stack.push(((x.wrapping_sub(1), y), next_face, new_d));
        }

        return cube;
    }

    fn rotate(tiles: &Vec<Vec<Tile>>, dir: i32) -> Vec<Vec<Tile>> {
        let mut result = tiles.clone();

        for _ in 0..dir.rem_euclid(4) {
            let mut iters = result.iter().map(|x| x.iter()).collect::<Vec<_>>();
            let mut transposed = Vec::new();

            while let Some(mut trans) = iters
                .iter_mut()
                .map(|it| it.next().map(|&x| x))
                .collect::<Option<Vec<_>>>()
            {
                trans.reverse();
                transposed.push(trans);
            }
            result = transposed;
        }
        return result;
    }

    fn wrap(face: Face, dir: Direction) -> (Face, Direction) {
        match (face, dir) {
            (Face::A, Right) => (Face::D, Down),
            (Face::A, Down) => (Face::C, Down),
            (Face::A, Left) => (Face::B, Down),
            (Face::A, Up) => (Face::F, Up),

            (Face::B, Right) => (Face::C, Right),
            (Face::B, Down) => (Face::E, Right),
            (Face::B, Left) => (Face::F, Right),
            (Face::B, Up) => (Face::A, Right),

            (Face::C, Right) => (Face::D, Right),
            (Face::C, Down) => (Face::E, Down),
            (Face::C, Left) => (Face::B, Left),
            (Face::C, Up) => (Face::A, Up),

            (Face::D, Right) => (Face::F, Left),
            (Face::D, Down) => (Face::E, Left),
            (Face::D, Left) => (Face::C, Left),
            (Face::D, Up) => (Face::A, Left),

            (Face::E, Right) => (Face::D, Up),
            (Face::E, Down) => (Face::F, Down),
            (Face::E, Left) => (Face::B, Up),
            (Face::E, Up) => (Face::C, Up),

            (Face::F, Right) => (Face::D, Left),
            (Face::F, Down) => (Face::A, Down),
            (Face::F, Left) => (Face::B, Right),
            (Face::F, Up) => (Face::E, Up),
        }
    }

    fn step(face: Face, pos: Position) -> (Face, Position) {
        let (x, y, k) = match pos.facing {
            Right => (pos.row, pos.column + 1, pos.row),
            Down => (pos.row + 1, pos.column, SIDELENGTH - 1 - pos.column),
            Left => (
                pos.row,
                pos.column.wrapping_sub(1),
                SIDELENGTH - 1 - pos.row,
            ),
            Up => (pos.row.wrapping_sub(1), pos.column, pos.column),
        };
        if x < SIDELENGTH && y < SIDELENGTH {
            return (
                face,
                Position {
                    row: x,
                    column: y,
                    facing: pos.facing,
                },
            );
        }

        let (next_face, next_dir) = Cube::wrap(face, pos.facing);
        let (next_row, next_col) = match next_dir {
            Right => (k, 0),
            Down => (0, SIDELENGTH - 1 - k),
            Left => (SIDELENGTH - 1 - k, SIDELENGTH - 1),
            Up => (SIDELENGTH - 1, k),
        };

        (
            next_face,
            Position {
                row: next_row,
                column: next_col,
                facing: next_dir,
            },
        )
    }
}

impl std::fmt::Debug for Cube {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let blank = std::iter::repeat(" ").take(SIDELENGTH).collect::<String>();

        for line in &self.faces[0] {
            write!(f, "{}", blank)?;
            for tile in line {
                write!(f, "{:?}", tile)?;
            }
            writeln!(f)?;
        }

        for l in 0..SIDELENGTH {
            for i in 1..=3 {
                for tile in &self.faces[i][l] {
                    write!(f, "{:?}", tile)?;
                }
            }
            writeln!(f)?;
        }

        for line in (&self.faces[4]).iter().chain((&self.faces[5]).iter()) {
            write!(f, "{}", blank)?;
            for tile in line {
                write!(f, "{:?}", tile)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

use std::str::FromStr;

impl FromStr for Tile {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "." => Ok(OpenTile),
            "#" => Ok(SolidWall),
            " " => Ok(EmptySpace),
            _ => Err(()),
        }
    }
}

impl FromStr for Command {
    type Err = std::num::ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "R" => Ok(TurnRight),
            "L" => Ok(TurnLeft),
            num => Ok(GoForward(num.parse()?)),
        }
    }
}

use Command::*;
use Direction::*;
use Tile::*;

fn input() -> (Vec<Vec<Tile>>, Vec<Command>) {
    let mut lines = std::io::stdin().lines().flatten();

    // parse the map
    let map = lines.by_ref().take_while(|l| !l.is_empty());
    let tiles: Vec<Vec<Tile>> = map
        .map(|line| {
            line.chars()
                .map(|c| c.to_string().parse().unwrap())
                .collect()
        })
        .collect();

    // parse the path
    use regex::Regex;
    let path = lines.next().unwrap();
    let re = Regex::new(r"\d+|R|L").unwrap();
    let commands = re
        .find_iter(path.as_str())
        .map(|command_match| command_match.as_str().parse().unwrap())
        .collect();

    (tiles, commands)
}
