use std::{fmt::Display, str::FromStr};

fn main() {
    let sum: i64 = std::io::stdin()
        .lines()
        .flatten()
        .map(|l| l.parse::<SNAFU>().unwrap().0)
        .sum();
    println!(
        "Those numbers sum to {}, which in SNAFU is {}",
        sum,
        SNAFU(sum),
    );
}

struct SNAFU(i64);

impl FromStr for SNAFU {
    type Err = std::num::IntErrorKind;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // todo: check for overflow, and return Err(PosOverflow) or Err(NegOverflow)
        use std::num::IntErrorKind::*;
        if s.is_empty() {
            return Err(Empty);
        }
        let mut sum = 0;
        for (i, d) in s.chars().rev().enumerate() {
            sum += 5i64.pow(i as u32)
                * match d {
                    '=' => Ok(-2),
                    '-' => Ok(-1),
                    '0' => Ok(0),
                    '1' => Ok(1),
                    '2' => Ok(2),
                    _ => Err(InvalidDigit),
                }?
        }

        Ok(SNAFU(sum))
    }
}

impl Display for SNAFU {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut num = self.0;
        let mut stack = String::new();
        while num != 0 {
            let r = num.rem_euclid(5);
            let d = num.div_euclid(5);
            stack.push(match r {
                0 => '0',
                1 => '1',
                2 => '2',
                3 => '=',
                4 => '-',
                _ => unreachable!(),
            });
            num = d;
            if r >= 3 {
                num += 1;
            }
        }
        write!(f, "{}", stack.chars().rev().collect::<String>())
    }
}
