use std::collections::HashSet;
use std::env;

fn main() {
    let args = env::args().skip(1).next();
    match args.as_deref() {
        Some("fst") => fst(),
        Some("snd") => snd(),
        _ => println!("specify either fst or snd as the first argument"),
    }
}

fn fst() {
    let total = std::io::stdin()
        .lines()
        .map(|a| a.unwrap())
        .fold(0, |acc, line| {
            let length = line.len();
            let (left, right) = line.split_at(length / 2);
            let lchars: HashSet<_> = left.chars().collect();
            let rchars: HashSet<_> = right.chars().collect();
            let char = HashSet::intersection(&lchars, &rchars).next().unwrap();
            acc + priority(*char)
        });
    println!("{}", total);
}

fn snd() {
    let lines: Vec<_> = std::io::stdin().lines().map(|a| a.unwrap()).collect();
    let chunks = lines.chunks(3);
    let total = chunks.fold(0, |acc, chunk| {
        let char = *chunk
            .iter()
            .fold(chunk[0].chars().collect(), |acc, line| {
                let chars: HashSet<char> = line.chars().collect();
                HashSet::intersection(&acc, &chars).map(|c| *c).collect()
            })
            .iter()
            .next()
            .unwrap();
        acc + priority(char)
    });
    println!("{}", total);
}

fn priority(c: char) -> u32 {
    let i: u32 = c.into();
    if c.is_lowercase() {
        let a: u32 = 'a'.into();
        i - a + 1
    } else {
        let a: u32 = 'A'.into();
        i - a + 27
    }
}
