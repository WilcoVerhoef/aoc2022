fn main() {
    let heightmap = read_input();
    let mut dists: Vec<Vec<i32>> = vec![vec![i32::MAX; heightmap[0].len()]; heightmap.len()];
    let mut start = (0, 0);
    let mut end = (0, 0);
    for (x, line) in heightmap.iter().enumerate() {
        for (y, &char) in line.iter().enumerate() {
            if char == 'S' {
                start = (x, y);
            }
            if char == 'E' {
                end = (x, y);
            }
        }
    }

    let actual_heightmap: Vec<Vec<char>> = heightmap
        .iter()
        .map(|row| {
            row.iter()
                .map(|c| match c {
                    'S' => 'a',
                    'E' => 'z',
                    c => *c,
                })
                .collect()
        })
        .collect();
    explore(&actual_heightmap, &mut dists, start, 0);
    println!("{:?}", dists);
    println!("{:?}", dists[end.0][end.1]);
}

fn explore(
    heightmap: &Vec<Vec<char>>,
    mut seen: &mut Vec<Vec<i32>>,
    (x, y): (usize, usize),
    depth: i32,
) {
    // println!("{:#?}", (x, y));
    if seen[x][y] <= depth {
        return;
    }

    seen[x][y] = depth;

    [
        (x.checked_add(1), Some(y)),
        (x.checked_sub(1), Some(y)),
        (Some(x), y.checked_add(1)),
        (Some(x), y.checked_sub(1)),
    ]
    .iter()
    .filter_map(|&next| match next {
        (Some(x), Some(y)) => Some((x, y)),
        _ => None,
    })
    .filter(|(x, y)| *x < heightmap.len() && *y < heightmap[0].len())
    .filter(|(x_, y_)| (heightmap[*x_][*y_] as i32) <= (heightmap[x][y] as i32) + 1)
    .map(|next| explore(&heightmap, &mut seen, next, depth + 1))
    .count();
}

fn read_input() -> Vec<Vec<char>> {
    let mut heightmap: Vec<Vec<char>> = Vec::new();

    let mut input = String::new();
    let stdin = std::io::stdin();
    while stdin.read_line(&mut input).unwrap() > 0 {
        let charvec = input.chars().collect();
        heightmap.push(charvec);
        input.clear();
    }

    return heightmap;
}
