type Droplets = Vec<(i32, i32, i32)>;
type Grid = [[[Cell; 20]; 20]; 20];

#[derive(Default, Clone, Copy, PartialEq)]
enum Cell {
    #[default]
    Air,
    Lava,
    Water,
}

use Cell::*;

fn main() {
    let droplets = input();

    // just out of curiosity, how full is this grid?
    let split_coords = droplets
        .iter()
        .flat_map(|&(x, y, z)| [x, y, z])
        .collect::<Vec<_>>();

    println!(
        "The minimum is {} and the max is {}",
        split_coords.iter().min().unwrap(),
        split_coords.iter().max().unwrap()
    );

    let mut grid = build_grid(&droplets);

    println!("I found {} sides!", count_sides(&droplets, &grid, false));

    flood(&mut grid);

    println!(
        "After flooding I found {} sides!",
        count_sides(&droplets, &grid, true)
    );
}

fn input() -> Droplets {
    std::io::stdin()
        .lines()
        .flatten()
        .map(|line| {
            let mut splitted = line.split(",");
            let mut number = || splitted.next().unwrap().parse().unwrap();
            (number(), number(), number())
        })
        .collect()
}

fn build_grid(droplets: &Droplets) -> Grid {
    let mut grid: Grid = Default::default();
    for &(x, y, z) in droplets {
        grid[x as usize][y as usize][z as usize] = Lava;
    }
    return grid;
}

fn count_sides(droplets: &Droplets, grid: &Grid, only_water: bool) -> i32 {
    droplets
        .iter()
        .map(|&(x, y, z)| {
            let get =
                |x: i32, y: i32, z: i32| grid.get(x as usize)?.get(y as usize)?.get(z as usize);
            6 - [
                get(x - 1, y, z),
                get(x + 1, y, z),
                get(x, y - 1, z),
                get(x, y + 1, z),
                get(x, y, z - 1),
                get(x, y, z + 1),
            ]
            .iter()
            .flatten()
            .map(|b| match b {
                Air => only_water as i32,
                Lava => 1,
                Water => 0,
            })
            .sum::<i32>()
        })
        .sum()
}

fn flood(grid: &mut Grid) {
    fn check(grid: &mut Grid, x: i32, y: i32, z: i32) -> Option<()> {
        if *grid.get(x as usize)?.get(y as usize)?.get(z as usize)? == Air {
            grid[x as usize][y as usize][z as usize] = Water;
            check(grid, x - 1, y, z);
            check(grid, x + 1, y, z);
            check(grid, x, y - 1, z);
            check(grid, x, y + 1, z);
            check(grid, x, y, z - 1);
            check(grid, x, y, z + 1);
        }
        Some(())
    }

    for i in 0..20 {
        for j in 0..20 {
            check(grid, 0, i, j);
            check(grid, 19, i, j);
            check(grid, i, 0, j);
            check(grid, i, 19, j);
            check(grid, i, j, 0);
            check(grid, i, j, 19);
        }
    }
}
