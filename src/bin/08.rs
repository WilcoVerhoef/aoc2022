fn main() {
    let bomen: Vec<Vec<_>> = std::io::stdin()
        .lines()
        .map(|l| {
            l.unwrap()
                .chars()
                .map(|c| c.to_digit(10).unwrap() as i32)
                .collect()
        })
        .collect();

    let mut seen = [[false; 99]; 99];

    let mut total = 0;

    enum Side {
        Left,
        Right,
        Top,
        Bottom,
    }

    for side in [Side::Left, Side::Right, Side::Top, Side::Bottom] {
        for i in 0..99 {
            let mut height = -1;
            for j in 0..99 {
                let x = match side {
                    Side::Left => j,
                    Side::Right => 98 - j,
                    Side::Top => i,
                    Side::Bottom => i,
                };
                let y = match side {
                    Side::Left => i,
                    Side::Right => i,
                    Side::Top => j,
                    Side::Bottom => 98 - j,
                };
                if bomen[x][y] > height {
                    height = bomen[x][y];
                    if !seen[x][y] {
                        total += 1;
                        seen[x][y] = true;
                    }
                }
            }
        }
    }
    println!("first: {}", total);

    let mut best = 0;
    for x in 0..99 {
        for y in 0..99 {
            let h = bomen[x][y];
            let mut right = 0;
            for x_ in x + 1..99 {
                right += 1;
                if bomen[x_][y] >= h {
                    break;
                }
            }
            let mut left = 0;
            for x_ in (0..x).rev() {
                left += 1;
                if bomen[x_][y] >= h {
                    break;
                }
            }
            let mut down = 0;
            for y_ in y + 1..99 {
                down += 1;
                if bomen[x][y_] >= h {
                    break;
                }
            }
            let mut up = 0;
            for y_ in (0..y).rev() {
                up += 1;
                if bomen[x][y_] >= h {
                    break;
                }
            }
            let score = right * left * down * up;
            if score > best {
                best = score;
            }
        }
    }
    println!("second {}", best);
}
