use std::collections::{BinaryHeap, HashSet};

// We will use A* to find our way to the goal
// Since blizzards follow a very predictable pattern,
// our state only consists of time & place of the Expedition

#[derive(Clone, Copy, Debug, Hash)]
struct State {
    pos: (usize, usize),
    time: usize,
}

impl State {
    fn is_safe(&self, blizzards: &Vec<Vec<Option<Blizzard>>>) -> bool {
        let r_mod = blizzards.len();
        let c_mod = blizzards[self.pos.0].len();
        blizzards[self.pos.0.modular_add(self.time, r_mod)][self.pos.1] != Some(Up)
            && blizzards[self.pos.0.modular_sub(self.time, r_mod)][self.pos.1] != Some(Down)
            && blizzards[self.pos.0][self.pos.1.modular_add(self.time, c_mod)] != Some(Left)
            && blizzards[self.pos.0][self.pos.1.modular_sub(self.time, c_mod)] != Some(Right)
    }

    fn neighbours(&self, last_row: usize, last_col: usize) -> Vec<Self> {
        let State { pos: (r, c), time } = *self;
        let mut neighs = Vec::new();

        neighs.push(State {
            pos: (r, c),
            time: time + 1,
        });
        if r > 0 {
            neighs.push(State {
                pos: (r - 1, c),
                time: time + 1,
            })
        }
        if c > 0 {
            neighs.push(State {
                pos: (r, c - 1),
                time: time + 1,
            })
        }
        if r < last_row {
            neighs.push(State {
                pos: (r + 1, c),
                time: time + 1,
            })
        }
        if c < last_col {
            neighs.push(State {
                pos: (r, c + 1),
                time: time + 1,
            })
        }

        neighs
    }
}

impl PartialEq for State {
    fn eq(&self, other: &Self) -> bool {
        self.pos.0 == other.pos.0 && self.pos.1 == other.pos.1 && self.time == other.time
    }
}

impl Eq for State {}

impl Ord for State {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let self_cost = self.time - self.pos.0 - self.pos.1;
        let other_cost = other.time - other.pos.0 - other.pos.1;
        other_cost
            .cmp(&self_cost)
            .then(self.pos.cmp(&other.pos))
            .then(self.time.cmp(&other.time))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

fn main() {
    let blizzards = parse_input();

    let last_row = blizzards.len() - 1;
    let last_col = blizzards[0].len() - 1;

    let heen = search(
        &blizzards,
        State {
            pos: (0, 0),
            time: 0,
        },
        (last_row, last_col),
    );
    println!("We can reach our goal at time {}", heen);
    let terug = search(
        &blizzards,
        State {
            pos: (last_row, last_col),
            time: heen,
        },
        (0, 0),
    );
    println!("We have come back to start at {}", terug);
    let heen = search(
        &blizzards,
        State {
            pos: (0, 0),
            time: terug,
        },
        (last_row, last_col),
    );
    println!("We can reach our goal again at time {}", heen);
    // okay, now for part 2
}

fn search(blizzards: &Vec<Vec<Option<Blizzard>>>, start: State, goal: (usize, usize)) -> usize {
    let last_row = blizzards.len() - 1;
    let last_col = blizzards[0].len() - 1;

    // Get the initial states in the priority queue.
    let repeats_after = blizzards.len().lcm(&blizzards[0].len());
    let mut state_heap: BinaryHeap<State> = (1..=repeats_after)
        .map(|t| State {
            pos: start.pos,
            time: start.time + t,
        })
        .filter(|state| state.is_safe(&blizzards))
        .collect();

    let mut seen = HashSet::new();

    while let Some(state) = state_heap.pop() {
        if state.pos == goal {
            return state.time + 1;
        }

        for &neigh in state
            .neighbours(last_row, last_col)
            .iter()
            .filter(|s| s.is_safe(&blizzards))
        {
            if seen.replace(neigh).is_none() {
                state_heap.push(neigh);
            }
        }
    }
    panic!("not found");
}

use num::Integer;
use Blizzard::*;

#[derive(PartialEq, Eq)]
enum Blizzard {
    Up,
    Down,
    Left,
    Right,
}

fn parse_input() -> Vec<Vec<Option<Blizzard>>> {
    let input_lines: Vec<String> = std::io::stdin().lines().flatten().collect();

    let mut blizzards = Vec::new();

    for r in 1..input_lines.len() - 1 {
        blizzards.push(Vec::new());
        for c in 1..input_lines[r].len() - 1 {
            blizzards[r - 1].push(match input_lines[r].chars().nth(c) {
                Some('.') => None,
                Some('^') => Some(Up),
                Some('v') => Some(Down),
                Some('<') => Some(Left),
                Some('>') => Some(Right),
                _ => panic!(),
            });
        }
    }

    return blizzards;
}

trait Modular {
    fn modular_add(self, other: Self, modulo: Self) -> Self;
    fn modular_sub(self, other: Self, modulo: Self) -> Self;
}

impl Modular for usize {
    fn modular_add(self, other: Self, modulo: Self) -> Self {
        (self.rem_euclid(modulo) + other.rem_euclid(modulo)) % modulo
    }

    fn modular_sub(self, other: Self, modulo: Self) -> Self {
        (self.rem_euclid(modulo) + modulo - other.rem_euclid(modulo)) % modulo
    }
}
