fn main() {
    let mut lines = std::io::stdin().lines();
    let mut register: i32 = 1;
    let mut timestep: i32 = 1;
    // let mut nextleap: i32 = 20;
    // let mut total: i32 = 0;
    while let Some(Ok(command)) = lines.next() {
        let mut splitted = command.split(" ");
        if ((((timestep - 1) % 40) + 1) % 40).abs_diff(register + 1) <= 1 {
            print!("#");
        } else {
            print!(".");
        }
        match splitted.next() {
            Some("noop") => {
                timestep += 1;
                // if timestep >= nextleap {
                //     total += nextleap * register;
                //     nextleap += 40;
                // }
            }
            Some("addx") => {
                timestep += 1;
                // if timestep >= nextleap {
                //     total += nextleap * register;
                //     nextleap += 40;
                // }

                if ((((timestep - 1) % 40) + 1) % 40).abs_diff(register + 1) <= 1 {
                    print!("#");
                } else {
                    print!(".");
                }
                // if (((timestep - 1) % 40) + 1) % 40 == 0 {
                //     print!("\n");
                // }
                let n: i32 = splitted.next().unwrap().parse().unwrap();
                register += n;
                timestep += 1;
                // if timestep >= nextleap {
                //     total += nextleap * register;
                //     nextleap += 40;
                // }
            }
            _ => unreachable!(),
        }
        // if (((timestep - 1) % 40) + 1) % 40 == 0 {
        //     print!("\n");
        // }
    }
    // println!("{}", total);
}
