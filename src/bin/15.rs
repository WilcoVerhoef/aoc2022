use std::{collections::HashSet, str::FromStr};

#[derive(Debug)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn distance(&self, other: &Position) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

#[derive(Debug)]
struct Sensor {
    position: Position,
    closest_beacon: Position,
}

impl FromStr for Sensor {
    type Err = std::num::ParseIntError;
    fn from_str(s: &str) -> Result<Sensor, std::num::ParseIntError> {
        let words = s.split(" ").collect::<Vec<_>>();

        let x = words[2]
            .trim_start_matches("x=")
            .trim_end_matches(",")
            .parse::<i32>()?;

        let y = words[3]
            .trim_start_matches("y=")
            .trim_end_matches(":")
            .parse::<i32>()?;

        let beacon_x = words[8]
            .trim_start_matches("x=")
            .trim_end_matches(",")
            .parse::<i32>()?;

        let beacon_y = words[9] //
            .trim_start_matches("y=")
            .parse::<i32>()?;

        Ok(Sensor {
            position: Position { x, y },
            closest_beacon: Position {
                x: beacon_x,
                y: beacon_y,
            },
        })
    }
}

#[derive(Default)]
struct RangeSet {
    ranges: Vec<(i32, i32)>,
}

impl RangeSet {
    fn new(start: i32, end: i32) -> RangeSet {
        RangeSet {
            ranges: vec![(start, end)],
        }
    }
    fn empty() -> RangeSet {
        RangeSet::default()
    }
    fn size(&self) -> i32 {
        self.ranges.iter().map(|(a, b)| b - a).sum()
    }
    fn union(&self, other: &RangeSet) -> RangeSet {
        // println!("Adding {:?}", other.ranges);
        let mut sort_ranges = Vec::new();
        sort_ranges.append(&mut self.ranges.clone());
        sort_ranges.append(&mut other.ranges.clone());
        sort_ranges.sort();

        // println!("sort {:?}", sort_ranges);

        // we now have sorted ranges, lets remove overlap!

        let mut range_iter = sort_ranges.iter().peekable();
        let mut ranges = Vec::new();

        while let Some(mut range) = range_iter.next().map(|x| *x) {
            while range_iter
                .peek()
                .map(|next| next.0 < range.1)
                .unwrap_or_default()
            {
                range.1 = range.1.max(range_iter.next().unwrap().1);
            }
            ranges.push(range);
        }

        // println!("dinges {:?}", ranges);
        RangeSet { ranges }
    }
}

fn main() {
    let lines = std::io::stdin().lines().filter_map(|x| x.ok());
    let sensors: Vec<Sensor> = lines.map(|x| x.parse::<Sensor>().unwrap()).collect();

    let bound = 4_000_000;
    for y_line in 3_100_000..bound {
        // let y_line = 2000000;
        // let y_line = 11;
        if y_line % 10_000 == 0 {
            println!("{}", y_line);
        }

        let mut asdf = HashSet::new();

        let sensor_ranges = sensors
            .iter()
            .map(|sensor| {
                if sensor.closest_beacon.y == y_line {
                    asdf.insert(sensor.closest_beacon.x);
                }
                let distance = Position::distance(&sensor.position, &sensor.closest_beacon);
                let y_dist = (sensor.position.y - y_line).abs();
                let stretch = distance - y_dist;
                // println!("{:?} {} {}", sensor, distance, stretch);
                if stretch >= 0 {
                    Some((sensor.position.x - stretch, sensor.position.x + stretch + 1))
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        // println!("{:?}", sensor_ranges);
        let sensor_ranges = sensor_ranges.iter().flatten().collect::<Vec<_>>();
        let line_ranges = sensor_ranges.iter().fold(RangeSet::empty(), |acc, cur| {
            acc.union(&RangeSet::new(cur.0.max(0), cur.1.min(bound)))
        });

        // println!("{:?}", line_ranges.ranges);
        if line_ranges.size() < bound {
            println!("{} - {}", line_ranges.size(), asdf.len());
            println!("{} {:?}", y_line, line_ranges.ranges);
        }
    }

    // second part
    println!("\n\n\nSECOND PART\n\n");

    for sens in sensors.iter() {
        let range = Position::distance(&sens.position, &sens.closest_beacon);

        for x_off in -range - 1..0 {
            let y_off = x_off + range + 1;

            consider(
                sens.position.x + x_off,
                sens.position.y + y_off,
                &sensors,
                bound,
            );

            let y_off = -x_off - range - 1;

            consider(
                sens.position.x + x_off,
                sens.position.y + y_off,
                &sensors,
                bound,
            );
        }
        for x_off in 0..range + 1 {
            let y_off = -range - 1 + x_off;

            consider(
                sens.position.x + x_off,
                sens.position.y + y_off,
                &sensors,
                bound,
            );

            let y_off = range + 1 - x_off;

            consider(
                sens.position.x + x_off,
                sens.position.y + y_off,
                &sensors,
                bound,
            );
        }
    }

    for sens_a in sensors.iter() {
        for sens_b in sensors.iter() {
            let sens_a_range = Position::distance(&sens_a.position, &sens_a.closest_beacon);
            let sens_b_range = Position::distance(&sens_b.position, &sens_b.closest_beacon);

            let sens_to_sens = Position::distance(&sens_a.position, &sens_b.position);
            if sens_to_sens > sens_a_range + sens_b_range + 2 {
                continue;
            }

            // println!("{:?} {:?}", sens_a.position, sens_b.position);

            // A positive edge, B negative edge

            for side_a in [-1, 1] {
                'point: for side_b in [-1, 1] {
                    let x = sens_a.position.x //.
                        - sens_a.position.y
                        - side_a * sens_a_range
                        + sens_b.position.x
                        + sens_b.position.y
                        + side_b * sens_b_range
                        - (side_a - side_b);
                    let y = -sens_a.position.x //.
                        + sens_a.position.y
                        + side_a * sens_a_range
                        + sens_b.position.x
                        + sens_b.position.y
                        + side_b * sens_b_range
                        + (side_a + side_b);

                    // let this_point = Position { x: x / 2, y: y / 2 };

                    consider(x / 2, y / 2, &sensors, bound);
                }
            }
            for side_a in [-1, 1] {
                'point: for side_b in [-1, 1] {
                    let x = sens_a.position.x //.
                        + sens_a.position.y
                        + side_a * sens_a_range
                        + sens_b.position.x
                        - sens_b.position.y
                        - side_b * sens_b_range
                        + (side_a - side_b);
                    let y = sens_a.position.x //.
                        + sens_a.position.y
                        + side_a * sens_a_range
                        - sens_b.position.x
                        + sens_b.position.y
                        + side_b * sens_b_range
                        + (side_a + side_b);

                    consider(x / 2, y / 2, &sensors, bound);
                }
            }
        }
    }
}

fn consider(x: i32, y: i32, sensors: &Vec<Sensor>, bound: i32) {
    for x_off in -5..6 {
        for y_off in -5..6 {
            let this_point = Position {
                x: x + x_off,
                y: y + y_off,
            };
            // check this point
            for sens_check in sensors.iter() {
                if this_point.distance(&sens_check.position)
                    <= sens_check.position.distance(&sens_check.closest_beacon)
                {
                    return;
                }
            }
            // We have found a spot!
            if this_point.x >= 0
                && this_point.x <= bound
                && this_point.y >= 0
                && this_point.y <= bound
            {
                println!("{:?}", this_point);
            }
        }
    }
}
