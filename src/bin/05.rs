use std::io;

fn main() {
    let lines = io::stdin().lines();
    let config: Vec<String> = lines
        .take_while(|line| !line.as_ref().unwrap().is_empty())
        .map(|a| a.unwrap())
        .collect();
    let mut stacks = parse_config(config);

    let mut lines2 = io::stdin().lines();
    while let Some(Ok(line)) = lines2.next() {
        // println!("{:?}", stacks);
        // println!("{}", line);
        execute_command(line, &mut stacks, false);
    }

    let asdf: String = stacks.iter().map(|stack| *stack.last().unwrap()).collect();
    println!("{}", asdf);
}

fn parse_config(input: Vec<String>) -> Vec<Vec<char>> {
    let mut lines = input.iter().rev();
    let n = lines.next().unwrap().chars().filter(|&x| x != ' ').count();

    let mut stacks = vec![Vec::new(); n];

    for line in lines {
        for i in 0..n {
            let c = line.chars().nth(1 + 4 * i).unwrap();
            if c != ' ' {
                stacks[i].push(c);
            }
        }
    }
    return stacks;
}

fn execute_command(command: String, stacks: &mut Vec<Vec<char>>, part2: bool) {
    let words: Vec<&str> = command.split(" ").collect();
    let amount: u32 = words[1].parse().unwrap();
    let from: usize = words[3].parse().unwrap();
    let to: usize = words[5].parse().unwrap();

    let mut temp = Vec::new();
    for _ in 0..amount {
        let item = stacks[from - 1].pop().unwrap();
        temp.push(item);
    }
    if !part2 {
        temp.reverse();
    }
    for _ in 0..amount {
        stacks[to - 1].push(temp.pop().unwrap());
    }
}
