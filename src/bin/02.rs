fn main() {
    let lines: Vec<_> = std::io::stdin().lines().collect();
    let total = lines
        .iter()
        .fold(0, |acc, new| acc + score(new.as_ref().unwrap().as_str()));
    println!("part 1: {}", total);
    let total2 = lines
        .iter()
        .fold(0, |acc, new| acc + score2(new.as_ref().unwrap()));
    println!("part 2: {}", total2);
}

fn score(line: &str) -> i32 {
    match line {
        "A X" => 1 + 3,
        "A Y" => 2 + 6,
        "A Z" => 3 + 0,
        "B X" => 1 + 0,
        "B Y" => 2 + 3,
        "B Z" => 3 + 6,
        "C X" => 1 + 6,
        "C Y" => 2 + 0,
        "C Z" => 3 + 3,
        _ => 0,
    }
}

fn score2(line: &String) -> i32 {
    match line.as_str() {
        "A X" => 3 + 0,
        "A Y" => 1 + 3,
        "A Z" => 2 + 6,
        "B X" => 1 + 0,
        "B Y" => 2 + 3,
        "B Z" => 3 + 6,
        "C X" => 2 + 0,
        "C Y" => 3 + 3,
        "C Z" => 1 + 6,
        _ => 0,
    }
}
