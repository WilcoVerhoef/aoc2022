# Advent of Code 2022

***DO NOT** look at the code, or run it. this might ruin the fun for you!*

This repo will contain my solutions for this years challenges.
It will be my first time using Rust, which is my excuse for why this code is so ugly.

## Running

`cat input25 | cargo run --bin 25` where `25` is the day.
